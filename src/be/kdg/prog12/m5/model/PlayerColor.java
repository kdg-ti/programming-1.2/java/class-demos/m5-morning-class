package be.kdg.prog12.m5.model;

public enum PlayerColor {
    RED, GREEN, BLUE, YELLOW
}
