package be.kdg.prog12.m5.model;

public class Pawn {
    private Tile currentTile; // could be null

    public Pawn() {
    }

    public Tile getCurrentTile() {
        return currentTile;
    }
}
