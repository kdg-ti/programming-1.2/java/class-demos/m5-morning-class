package be.kdg.prog12.m5.view.board;

import be.kdg.prog12.m5.model.LudoGame;
import be.kdg.prog12.m5.model.PlayerColor;
import be.kdg.prog12.m5.model.Tile;
import be.kdg.prog12.m5.view.stats.StatsPresenter;
import be.kdg.prog12.m5.view.stats.StatsView;
import javafx.scene.paint.Color;

public class BoardPresenter {
    private final LudoGame model;
    private final BoardView view;

    public BoardPresenter(LudoGame model, BoardView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getCanvas().setOnMouseMoved(mouseEvent -> {
            final double x = mouseEvent.getX();
            final double y = mouseEvent.getY();

            updateView();
            view.highlightMouseHover(x, y);
        });

        view.getCanvas().setOnMouseClicked(mouseEvent -> {
            StatsView statsView = new StatsView();
            StatsPresenter statsPresenter = new StatsPresenter(this.model, statsView);

            view.getScene().setRoot(statsView);
            statsView.getScene().getWindow().sizeToScene();
        });
    }

    private void updateView() {
        // Step 1: 'clear' the background ('reset' the Canvas)
        view.drawBackground();

        // Step 2: ask the model for each of the pawns
        // TODO: call the model, make this dynamic
        view.drawPawn(0, colorToJavaFXColor(PlayerColor.BLUE));
        view.drawPawn(1, colorToJavaFXColor(PlayerColor.RED));

        // Step 3: hightlighted tiles
        for (Tile tile : model.getHighlightedTiles()) {
            view.highlightTile(
                    tile.getTileIndex(),
                    // TODO: color should be pawn's of player's color
                    colorToJavaFXColor(PlayerColor.GREEN));
        }
    }

    private Color colorToJavaFXColor(PlayerColor playerColor) {
        switch (playerColor) {
            case RED:
                return Color.RED;
            case GREEN:
                return Color.GREEN;
            case BLUE:
                return Color.BLUE;
            case YELLOW:
                return Color.YELLOW;
            default:
                throw new IllegalArgumentException("Invalid color: " + playerColor);
        }
    }
}
