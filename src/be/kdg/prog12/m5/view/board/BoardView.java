package be.kdg.prog12.m5.view.board;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

public class BoardView extends BorderPane {
    private Canvas canvas;
    private static final double BOARD_WIDTH = 1300.0;
    private static final double BOARD_HEIGHT = 1300.0;
    private static final double CIRCLE_DIAMETER = 70.0;
    private static final double HIGHLIGHT_DIAMETER = 90.0;
    private static final double HIGHLIGHT_BORDER_WIDTH = 10.0;

    private final Image ludoBoard;

    private final TileCoordinates[] tileCoordinates
            = {
            new TileCoordinates(108.1, 541.7), // CENTER of the first tile
            new TileCoordinates(216.5, 541.7), // CENTER of the second tile
    };

    private static class TileCoordinates {
        private final double x;
        private final double y;

        public TileCoordinates(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }
    }

    public BoardView() {
        ludoBoard = new Image("/ludo.jpg");

        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        canvas = new Canvas(BOARD_WIDTH, BOARD_HEIGHT);
    }

    private void layoutNodes() {
        setCenter(canvas);
    }

    Canvas getCanvas() {
        return canvas;
    }

    void drawBackground() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.drawImage(ludoBoard, 0.0, 0.0, BOARD_WIDTH, BOARD_HEIGHT);

        /*
        gc.setFill(Color.AQUA);
        gc.fillRect(0.0, 0.0, BOARD_WIDTH, BOARD_HEIGHT);

        gc.setStroke(Color.PURPLE);
        gc.setLineWidth(5.0);
        gc.strokeLine(100.0, 100.0, 100.0, BOARD_HEIGHT - 100.0);
        */
    }

    void drawPawn(int tileIndex, Color color) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(color);

        gc.fillOval(
                tileCoordinates[tileIndex].getX() - CIRCLE_DIAMETER / 2,
                tileCoordinates[tileIndex].getY() - CIRCLE_DIAMETER / 2,
                CIRCLE_DIAMETER,
                CIRCLE_DIAMETER);
    }

    void highlightTile(int tileIndex, Color color) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setStroke(color);
        gc.setLineWidth(HIGHLIGHT_BORDER_WIDTH);

        gc.strokeOval(
                tileCoordinates[tileIndex].getX() - HIGHLIGHT_DIAMETER / 2,
                tileCoordinates[tileIndex].getY() - HIGHLIGHT_DIAMETER / 2,
                HIGHLIGHT_DIAMETER,
                HIGHLIGHT_DIAMETER);
    }

    void highlightMouseHover(double x, double y) {
        for (TileCoordinates tileCoordinates : tileCoordinates) {
            double distanceToTileCenter
                    = Math.sqrt(
                    Math.pow(x - tileCoordinates.getX(), 2.0)
                            + Math.pow(y - tileCoordinates.getY(), 2.0)
            );

            if (distanceToTileCenter <= CIRCLE_DIAMETER / 2) {
                // Here's some code duplication (see 'highlightTile')
                // TODO: fix this (Don't Repeat Yourself ... D.R.Y.)
                GraphicsContext gc = canvas.getGraphicsContext2D();
                gc.setFill(Color.BLACK);

                gc.fillOval(
                        tileCoordinates.getX() - CIRCLE_DIAMETER / 2,
                        tileCoordinates.getY() - CIRCLE_DIAMETER / 2,
                        CIRCLE_DIAMETER,
                        CIRCLE_DIAMETER);
            }
        }
    }
}
