package be.kdg.prog12.m5;

import be.kdg.prog12.m5.model.LudoGame;
import be.kdg.prog12.m5.view.board.BoardPresenter;
import be.kdg.prog12.m5.view.board.BoardView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class LudoApplication extends Application {
    @Override
    public void start(Stage stage) {
        LudoGame model = new LudoGame();
        BoardView view = new BoardView();
        BoardPresenter presenter = new BoardPresenter(model, view);

        Scene scene = new Scene(view);

        stage.setScene(scene);
        stage.setTitle("Ludo");
        stage.sizeToScene();
        stage.show();
    }
}
